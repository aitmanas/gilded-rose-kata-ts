package com.gildedrose.controller;

import com.gildedrose.domain.ItemEntity;
import com.gildedrose.dto.Item;
import com.gildedrose.service.ItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ItemService itemService;

    @Test
    public void test_item_list() throws Exception {
        List<ItemEntity> items = new ArrayList<>();
        items.add(new ItemEntity("id1", new Item("name1", 1, 2)));
        items.add(new ItemEntity("id2", new Item("name2", 3, 4)));

        given(itemService.findAll()).willReturn(items);

        mvc.perform(get("/item/list").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(items.get(0).getId())))
                .andExpect(jsonPath("$[0].name", is(items.get(0).getName())))
                .andExpect(jsonPath("$[0].sellIn", is(items.get(0).getSellIn())))
                .andExpect(jsonPath("$[0].quality", is(items.get(0).getQuality())))
                .andExpect(jsonPath("$[1].id", is(items.get(1).getId())))
                .andExpect(jsonPath("$[1].name", is(items.get(1).getName())))
                .andExpect(jsonPath("$[1].sellIn", is(items.get(1).getSellIn())))
                .andExpect(jsonPath("$[1].quality", is(items.get(1).getQuality())));
    }

}
