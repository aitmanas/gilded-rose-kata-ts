package com.gildedrose.controller;

import com.gildedrose.domain.ItemEntity;
import com.gildedrose.service.ItemService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ItemControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ItemService itemService;

    @Before
    public void setup() {
        itemService.deleteAll();
    }

    @Test
    public void test_item_list() throws Exception {
        itemService.save(new ItemEntity("name1", 1, 2));
        itemService.save(new ItemEntity("name2", 3, 4));

        mvc.perform(get("/item/list").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", startsWith("name")))
                .andExpect(jsonPath("$[0].sellIn", anyOf(is(1), is(3))))
                .andExpect(jsonPath("$[0].quality", anyOf(is(2), is(4))))
                .andExpect(jsonPath("$[1].name", startsWith("name")))
                .andExpect(jsonPath("$[1].sellIn", anyOf(is(1), is(3))))
                .andExpect(jsonPath("$[1].quality", anyOf(is(2), is(4))));
    }

}
