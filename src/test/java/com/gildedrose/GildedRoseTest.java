package com.gildedrose;

import static org.junit.Assert.*;

import com.gildedrose.dto.Item;
import org.junit.Test;

public class GildedRoseTest {


    @Test
    public void sellIn_value_lowered_by_1_for_non_Sulfuras_items() {
        Item[] items = new Item[] {
                new Item("any item 1", 0, 0),
                new Item("any item 2", 2, 0),
                new Item("any item 3", -2, 0),
                new Item("any item 4", 10, 0)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(-1, app.items[0].sellIn);
        assertEquals(1, app.items[1].sellIn);
        assertEquals(-3, app.items[2].sellIn);
        assertEquals(9, app.items[3].sellIn);
    }


    @Test
    public void sellIn_value_unchanged_for_Sulfuras_items() {
        Item[] items = new Item[] {
                new Item("Sulfuras, Hand of Ragnaros", 0, 10),
                new Item("Sulfuras, Hand of Ragnaros", -10, 5),
                new Item("Sulfuras, Hand of Ragnaros", 10, 0),
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(0, app.items[0].sellIn);
        assertEquals(-10, app.items[1].sellIn);
        assertEquals(10, app.items[2].sellIn);
    }


    @Test
    public void quality_value_unchanged_for_Sulfuras_items() {
        Item[] items = new Item[] {
                new Item("Sulfuras, Hand of Ragnaros", 0, 10),
                new Item("Sulfuras, Hand of Ragnaros", -10, 5),
                new Item("Sulfuras, Hand of Ragnaros", 10, 0),
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(10, app.items[0].quality);
        assertEquals(5, app.items[1].quality);
        assertEquals(0, app.items[2].quality);
    }


    @Test
    public void quality_value_lowered_by_1_up_to_0_for_usual_items_with_positive_sellIn() {
        Item[] items = new Item[] {
                new Item("any item 1", 2, 0),
                new Item("any item 2", 3, 1),
                new Item("any item 3", 4, 10)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(0, app.items[0].quality);
        assertEquals(0, app.items[1].quality);
        assertEquals(9, app.items[2].quality);
    }


    @Test
    public void quality_value_lowered_by_2_up_to_0_for_usual_items_with_non_positive_sellIn() {
        Item[] items = new Item[] {
                new Item("any item 1", 0, 10),
                new Item("any item 2", -1, 10),
                new Item("any item 3", -1, 1)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(8, app.items[0].quality);
        assertEquals(8, app.items[1].quality);
        assertEquals(0, app.items[2].quality);
    }


    @Test
    public void quality_value_increased_by_1_up_to_50_for_aged_brie_items_with_positive_sellIn() {
        Item[] items = new Item[] {
                new Item("Aged Brie", 2, 10),
                new Item("Aged Brie", 3, 48),
                new Item("Aged Brie", 3, 49),
                new Item("Aged Brie", 4, 0)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(11, app.items[0].quality);
        assertEquals(49, app.items[1].quality);
        assertEquals(50, app.items[2].quality);
        assertEquals(1, app.items[3].quality);
    }


    @Test
    public void quality_value_increased_by_2_up_to_50_for_aged_brie_items_with_non_positive_sellIn() {
        Item[] items = new Item[] {
                new Item("Aged Brie", 0, 10),
                new Item("Aged Brie", -1, 48),
                new Item("Aged Brie", -1, 49),
                new Item("Aged Brie", -1, 0)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(12, app.items[0].quality);
        assertEquals(50, app.items[1].quality);
        assertEquals(50, app.items[2].quality);
        assertEquals(2, app.items[3].quality);
    }


    @Test
    public void quality_value_increased_by_1_for_backstage_pass_items_with_sellIn_above_10() {
        Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 11, 10),
                new Item("Backstage passes to a TAFKAL80ETC concert", 12, 0)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(11, app.items[0].quality);
        assertEquals(1, app.items[1].quality);
    }


    @Test
    public void quality_value_increased_by_2_for_backstage_pass_items_with_sellIn_from_10_to_6() {
        Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 10),
                new Item("Backstage passes to a TAFKAL80ETC concert", 6, 0)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(12, app.items[0].quality);
        assertEquals(2, app.items[1].quality);
    }


    @Test
    public void quality_value_increased_by_3_for_backstage_pass_items_with_sellIn_from_5_to_1() {
        Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 10),
                new Item("Backstage passes to a TAFKAL80ETC concert", 1, 0)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(13, app.items[0].quality);
        assertEquals(3, app.items[1].quality);
    }


    @Test
    public void quality_value_0_for_backstage_pass_items_with_sellIn_0_or_less() {
        Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10),
                new Item("Backstage passes to a TAFKAL80ETC concert", -1, 1)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(0, app.items[0].quality);
        assertEquals(0, app.items[1].quality);
    }


    @Test
    public void quality_value_lowered_by_2_up_to_0_for_conjured_items_with_positive_sellIn() {
        Item[] items = new Item[] {
                new Item("Conjured Mana Cake", 2, 0),
                new Item("Conjured Mana Cake", 3, 2),
                new Item("Conjured Mana Cake", 4, 10)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(0, app.items[0].quality);
        assertEquals(0, app.items[1].quality);
        assertEquals(8, app.items[2].quality);
    }


    @Test
    public void quality_value_lowered_by_4_up_to_0_for_conjured_items_with_non_positive_sellIn() {
        Item[] items = new Item[] {
                new Item("Conjured Mana Cake", 0, 10),
                new Item("Conjured Mana Cake", -1, 10),
                new Item("Conjured Mana Cake", -1, 3)
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(6, app.items[0].quality);
        assertEquals(6, app.items[1].quality);
        assertEquals(0, app.items[2].quality);
    }
}
