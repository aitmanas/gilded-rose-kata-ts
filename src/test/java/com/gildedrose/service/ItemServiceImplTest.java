package com.gildedrose.service;

import com.gildedrose.GildedRose;
import com.gildedrose.domain.ItemEntity;
import com.gildedrose.dto.Item;
import com.gildedrose.repository.ItemRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class ItemServiceImplTest {

    @TestConfiguration
    static class ItemServiceImplTestContextConfiguration {

        @Bean
        public ItemServiceImpl itemServiceSpy() {
            return spy(new ItemServiceImpl());
        }
    }

    @Autowired
    private ItemServiceImpl itemService;

    @MockBean
    private ItemRepository itemRepository;

    @Test
    public void test_save() {
        ItemEntity itemEntity = new ItemEntity();

        itemService.save(itemEntity);

        verify(itemRepository, times(1)).save(itemEntity);
    }

    @Test
    public void test_deleteAll() {
        itemService.deleteAll();

        verify(itemRepository, times(1)).deleteAll();
    }

    @Test
    public void test_findAll() {
        List<ItemEntity> items = new ArrayList<>();
        items.add(new ItemEntity("name1", 10, 10));
        items.add(new ItemEntity("name2", 9, 11));

        given(itemRepository.findAll()).willReturn(items);

        List<ItemEntity> result = itemService.findAll();

        assertEquals(items.get(0).getName(), result.get(0).getName());
        assertEquals(items.get(1).getName(), result.get(1).getName());
    }

    @Test
    public void test_updateQuality() {
        List<ItemEntity> items = new ArrayList<>();
        items.add(new ItemEntity("id1", new Item("name1", 10, 10)));
        items.add(new ItemEntity("id2", new Item("name2", 10, 10)));

        GildedRose gildedRoseMock = mock(GildedRose.class);

        given(itemRepository.findAll()).willReturn(items);
        given(itemService.makeGildedRose(any())).willReturn(gildedRoseMock);

        itemService.updateQuality();

        verify(itemRepository, times(1)).findAll();
        verify(gildedRoseMock, times(1)).updateQuality();
        verify(itemRepository, times(items.size())).save(any(ItemEntity.class));
    }

}
