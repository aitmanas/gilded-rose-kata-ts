package com.gildedrose.updater;

import com.gildedrose.dto.Item;
import com.gildedrose.util.Util;

public class AgedBrieItemUpdater extends ItemUpdater {
    @Override
    void updateQuality(Item item) {
        item.quality = Util.getBoundedValue(item.quality + getCommonDelta(item));
    }
}
