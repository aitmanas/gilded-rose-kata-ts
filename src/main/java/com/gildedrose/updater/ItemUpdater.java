package com.gildedrose.updater;

import com.gildedrose.dto.Item;

public abstract class ItemUpdater {

    public void update(Item item) {
        updateQuality(item);
        updateSellIn(item);
    }

    abstract void updateQuality(Item item);

    void updateSellIn(Item item) {
        item.sellIn--;
    }

    int getCommonDelta(Item item) {
        return item.sellIn < 1 ? 2 : 1;
    }
}
