package com.gildedrose.updater;

import com.gildedrose.dto.Item;
import com.gildedrose.util.Util;

public class BackstagePassesItemUpdater extends ItemUpdater {
    @Override
    void updateQuality(Item item) {
        int delta = 1;
        if (item.sellIn < 6) {
            delta = 3;
        } else if (item.sellIn < 11) {
            delta = 2;
        }
        item.quality = Util.getBoundedValue(item.sellIn < 1 ? 0 : item.quality + delta);
    }
}
