package com.gildedrose.updater;

import com.gildedrose.dto.ItemType;
import com.gildedrose.updater.ItemUpdater;

public class ItemUpdaterResolver {

    public static ItemUpdater getUpdater(String itemName) {
        String name = itemName != null ? itemName.toLowerCase() : "";
        for (ItemType itemType : ItemType.values()) {
            if (name.startsWith(itemType.prefix())) {
                return itemType.updater();
            }
        }
        return ItemType.OTHER.updater();
    }
}
