package com.gildedrose.controller;

import com.gildedrose.domain.ItemEntity;
import com.gildedrose.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RestController
public class ItemController {

    @Autowired
    ItemService itemService;

    @RequestMapping("/item/list")
    public List<ItemEntity> list() {
        return itemService.findAll();
    }

}