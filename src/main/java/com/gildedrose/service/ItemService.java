package com.gildedrose.service;

import com.gildedrose.domain.ItemEntity;

import java.util.List;

public interface ItemService {

    ItemEntity save(ItemEntity itemEntity);

    List<ItemEntity> findAll();

    void deleteAll();

    void updateQuality();
}
