package com.gildedrose.service;

import com.gildedrose.GildedRose;
import com.gildedrose.domain.ItemEntity;
import com.gildedrose.dto.Item;
import com.gildedrose.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public ItemEntity save(ItemEntity itemEntity) {
        return itemRepository.save(itemEntity);
    }

    @Override
    public List<ItemEntity> findAll() {
        List<ItemEntity> items = new ArrayList<>();
        itemRepository.findAll().forEach(items::add);
        return items;
    }

    @Override
    public void deleteAll() {
        itemRepository.deleteAll();
    }

    @Override
    @Scheduled(cron = "${item.updateQuality.cron.expression}")
    public void updateQuality() {
        Map<String, Item> itemsMap = new HashMap<>();
        itemRepository.findAll().forEach(itemEntity ->
                itemsMap.put(itemEntity.getId(), itemEntity.toItem())
        );

        Item[] items = itemsMap.values().toArray(new Item[itemsMap.values().size()]);
        makeGildedRose(items).updateQuality();

        itemsMap.forEach((id, item) ->
                itemRepository.save(new ItemEntity(id, item))
        );
    }

    GildedRose makeGildedRose(Item[] items) {
        return new GildedRose(items);
    }
}
