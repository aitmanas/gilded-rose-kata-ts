package com.gildedrose.dto;

import com.gildedrose.updater.*;

public enum ItemType {
    AGED_BRIE("aged brie", new AgedBrieItemUpdater()),
    BACKSTAGE_PASSES("backstage passes", new BackstagePassesItemUpdater()),
    CONJURED("conjured", new ConjuredItemUpdater()),
    SULFURAS("sulfuras", new SulfurasItemUpdater()),
    OTHER("", new DefaultItemUpdater());

    private String prefix;
    private ItemUpdater updater;

    ItemType(String prefix, ItemUpdater updater) {
        this.prefix = prefix;
        this.updater = updater;
    }

    public String prefix() {
        return prefix;
    }

    public ItemUpdater updater() {
        return updater;
    }
}
