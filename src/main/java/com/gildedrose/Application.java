package com.gildedrose;

import com.gildedrose.domain.ItemEntity;
import com.gildedrose.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application implements CommandLineRunner {

    @Autowired
    private ItemService itemService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        itemService.deleteAll();

        itemService.save(new ItemEntity("+5 Dexterity Vest", 10, 20));
        itemService.save(new ItemEntity("Aged Brie", 2, 0));
        itemService.save(new ItemEntity("Elixir of the Mongoose", 5, 7));
        itemService.save(new ItemEntity("Sulfuras, Hand of Ragnaros", 0, 80));
        itemService.save(new ItemEntity("Sulfuras, Hand of Ragnaros", -1, 80));
        itemService.save(new ItemEntity("Backstage passes to a TAFKAL80ETC concert", 15, 20));
        itemService.save(new ItemEntity("Backstage passes to a TAFKAL80ETC concert", 10, 49));
        itemService.save(new ItemEntity("Backstage passes to a TAFKAL80ETC concert", 5, 49));
        itemService.save(new ItemEntity("Conjured Mana Cake", 3, 6));

        itemService.findAll().forEach(System.out::println);
    }

}
