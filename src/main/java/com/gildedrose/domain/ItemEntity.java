package com.gildedrose.domain;

import com.gildedrose.dto.Item;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "index", type = "items")
public class ItemEntity {

    @Id
    private String id;

    private String name;

    private int sellIn;

    private int quality;


    public ItemEntity() {}

    public ItemEntity(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    public ItemEntity(String id, Item item) {
        this.id = id;
        this.name = item.name;
        this.sellIn = item.sellIn;
        this.quality = item.quality;
    }

    public Item toItem() {
        return new Item(name, sellIn, quality);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSellIn() {
        return sellIn;
    }

    public void setSellIn(int sellIn) {
        this.sellIn = sellIn;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    @Override
    public String toString() {
        return "ItemEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", sellIn=" + sellIn +
                ", quality=" + quality +
                '}';
    }
}
