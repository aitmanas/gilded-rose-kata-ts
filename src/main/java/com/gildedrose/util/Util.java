package com.gildedrose.util;

import static com.gildedrose.util.Constants.DEFAULT_LOWER_BOUND_FOR_QUALITY;
import static com.gildedrose.util.Constants.DEFAULT_UPPER_BOUND_FOR_QUALITY;

public class Util {

    public static int getBoundedValue(int value) {
        return getBoundedValue(value, DEFAULT_LOWER_BOUND_FOR_QUALITY, DEFAULT_UPPER_BOUND_FOR_QUALITY);
    }

    public static int getBoundedValue(int value, int lowerBound, int upperBound) {
        if (value > upperBound) {
            return upperBound;
        } else if (value < lowerBound) {
            return lowerBound;
        }
        return value;
    }
}
