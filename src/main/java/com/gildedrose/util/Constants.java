package com.gildedrose.util;

class Constants {

    static final int DEFAULT_LOWER_BOUND_FOR_QUALITY = 0;

    static final int DEFAULT_UPPER_BOUND_FOR_QUALITY = 50;

}
