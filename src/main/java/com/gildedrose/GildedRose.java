package com.gildedrose;

import com.gildedrose.dto.Item;
import com.gildedrose.updater.ItemUpdaterResolver;

import java.util.Arrays;

public class GildedRose {

    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        Arrays.stream(items).parallel().forEach(item ->
            ItemUpdaterResolver.getUpdater(item.name).update(item)
        );
    }
}